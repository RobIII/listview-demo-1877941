﻿using System;
using System.Collections.Concurrent;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class VirtualForm : Form
    {
        // Cache items; key = index in listview, value = item
        private ConcurrentDictionary<int, CustomerListViewItem> _cache;

        public VirtualForm()
        {
            InitializeComponent();

            // Set up retrieval of listviewitems
            MainListView.RetrieveVirtualItem += (s, e) =>
            {
                // Check cache, if not in cache create item on demand
                e.Item = _cache.GetOrAdd(e.ItemIndex, (i) =>
                {
                    // Do the work for 1 item here...
                    return new CustomerListViewItem(Customer.CreateRandomCustomer(i));
                });
            };
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            AddCustomers(2500);
        }

        public void AddCustomers(int count)
        {
            _cache = new ConcurrentDictionary<int, CustomerListViewItem>(Environment.ProcessorCount, count);
            MainListView.VirtualListSize = count;
            MainListView.Refresh();
        }
    }
}
