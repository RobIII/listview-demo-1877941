﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class BackgroundworkerForm : Form
    {
        private BackgroundWorker _bgw;

        public BackgroundworkerForm()
        {
            InitializeComponent();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            AddCustomers(2500);
        }

        private void AbortButton_Click(object sender, EventArgs e)
        {
            _bgw?.CancelAsync();
        }

        private void AddCustomers(int count)
        {
            // If we currently have no backgroundworker
            if (_bgw == null)
            {
                // Create a backgroundworker, set it up and kick it off
                using (_bgw = new BackgroundWorker() { WorkerReportsProgress = true, WorkerSupportsCancellation = true })
                {
                    _bgw.DoWork += DoWork;
                    _bgw.RunWorkerCompleted += WorkCompleted;
                    _bgw.ProgressChanged += (s, pe) => { MainProgressBar.Value = pe.ProgressPercentage; };
                    _bgw.RunWorkerAsync(count);
                }
            }
            else
            {
                throw new Exception("Already in progress");
            }
        }

        private void DoWork(object sender, DoWorkEventArgs e)
        {
            // Get reference to ourself and prepare a list of results
            var self = (BackgroundWorker)sender;
            var custcount = (int)e.Argument;
            var results = new List<Customer>(custcount);
            // Keep track of last reported progress
            var lastreportedprogress = 0;
            // Add "count" customers while not cancellation pending
            for (var i = 0; i < custcount && !self.CancellationPending; i++)
            {
                // Do actual work
                results.Add(Customer.CreateRandomCustomer(i));

                // Keep track of progress
                var progress = (int)((double)i / custcount * 100);
                // If the actual percentage changed, report it
                if (progress > lastreportedprogress)
                {
                    self.ReportProgress(progress);
                    lastreportedprogress = progress;
                }
            }
            // If not cancelled, report last status as 100
            if (!self.CancellationPending)
                self.ReportProgress(100);

            // Return whether the action was cancelled
            e.Cancel = self.CancellationPending;
            // Return results
            e.Result = results;
        }

        private void WorkCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Populate our listview with results (if any)
            MainListView.BeginUpdate();
            MainListView.Items.Clear();
            if (!e.Cancelled)
            {
                var results = (IEnumerable<Customer>)e.Result;
                MainListView.Items.AddRange(results.Select(r => new CustomerListViewItem(r)).ToArray());
            }
            MainListView.EndUpdate();
            // Reset progressbar
            MainProgressBar.Value = 0;
            // Clear backgroundworker so we're ready for a new run
            _bgw = null;
        }

    }
}
