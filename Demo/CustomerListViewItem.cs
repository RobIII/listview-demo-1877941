﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public class CustomerListViewItem : ListViewItem
    {
        public Customer Customer { get; set; }
        public CustomerListViewItem(Customer customer)
            : base(new[] { customer?.FirstName, customer?.LastName, customer?.DateOfBirth.ToString("yyyy-MM-dd") })
        {
            Customer = customer ?? throw new ArgumentNullException(nameof(customer));
        }
    }
}
