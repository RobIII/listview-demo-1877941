﻿using System;
using System.Threading;

namespace WindowsFormsApp1
{
    public class Customer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }

        private static readonly Random _rng = new Random();
        public static Customer CreateRandomCustomer(int id)
        {
            Thread.Sleep(1); // Fake some work
            return new Customer
            {
                Id = id,
                DateOfBirth = new DateTime(1900, 1, 1).AddDays(_rng.Next(365 * 100)),
                FirstName = "Bob" + _rng.Next(99999),
                LastName = "Doe" + _rng.Next(99999)
            };

        }
    }
}
