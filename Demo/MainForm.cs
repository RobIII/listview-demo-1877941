﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            new BackgroundworkerForm().Show();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            new VirtualForm().Show();
        }
    }
}
